﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="20008000">
	<Property Name="EndevoGOOP_ColorFrame" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorHeader" Type="UInt">6062466</Property>
	<Property Name="EndevoGOOP_ColorPrivate" Type="UInt">16777215</Property>
	<Property Name="EndevoGOOP_ColorPublic" Type="UInt">16776704</Property>
	<Property Name="EndevoGOOP_ColorTextBody" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_ColorTextHeader" Type="UInt">0</Property>
	<Property Name="EndevoGOOP_FrameThickness" Type="UInt">1</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)6!!!*Q(C=\&gt;4.&lt;=*!%-8RFSC(8/EAII6JA2:I96LAGC-NP":IA2;G"6KA!#\E\]X%)F)%FU2#%&lt;OMAZ`XYZ?VM44+K`3C][VSOFKY0PJ](K&lt;0_3M[@?;&gt;8BYOTXY9@ZKD=Y]`82U`8Z`(@J`NOP^'U;-]SP]O\T&gt;`"(`_/XD3^#+C&amp;=WUJ)7G:=^&amp;8O2&amp;8O2&amp;8O1G.\H*47ZSES&gt;ZEC&gt;ZEC&gt;ZEA&gt;ZE!&gt;ZE!&gt;ZE)^+,H+2CRR3M8CR5$&amp;J-5(2'9K+1_%J0)7H](#KQF.Y#E`B+4RU5?%J0)7H]"1?BKHQ&amp;*\#5XA+$V-.39V+DK@Q-,W-RXC-RXC-BS6F0!:A&amp;D-4GUFAS&amp;QU8YT(?)S(LT)?YT%?YT%?,MNYD-&gt;YD-&gt;Y'$*WR5-T68)]4+0%EXA34_**0%SNR*.Y%E`C34QMJ]34?"*%MG!S/11FAZ)/S5HC34T]5?**0)EH]31?,IU\F'.H*MV5S@%%HM!4?!*0Y'%+":\!%XA#4_"B7A7?Q".Y!E`A93E&amp;HM!4?!*)M#D,+ZAM'"BU#I,!QW=],4(OEI=E2J8[Y65`F/K(4@U1K2]/^5V8XUTV46*PPHJ4V:OFXA4V0[&gt;'KT(K2&gt;3$JYY[=DT1^L1&gt;&lt;5P&lt;U.;U&amp;7V*7UR$@\HD]8D5Y8$1@L`8&lt;L@4&gt;LP6:L02?LX7;L83=LH59L'98Q.PV0G&amp;=!`PJ&lt;MQ0/NS$@&lt;I!Y*P\8-!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="typeDef" Type="Folder">
			<Item Name="imageInfo.ctl" Type="VI" URL="../imageInfo.ctl"/>
		</Item>
		<Item Name="agregarImagen.vi" Type="VI" URL="../agregarImagen.vi"/>
		<Item Name="lecturaImagen.vi" Type="VI" URL="../lecturaImagen.vi"/>
	</Item>
	<Item Name="lecturaPNG.vi" Type="VI" URL="../lecturaPNG.vi"/>
</Library>
